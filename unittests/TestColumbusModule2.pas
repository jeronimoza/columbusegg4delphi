unit TestColumbusModule2;

interface

uses
  DUnitX.TestFramework,
  FireDAC.Comp.Client,
  ColumbusModule2.CustomersU,
  System.IOUtils;

type

  [TestFixture]
  TColumbusDatamodulesTests = class(TObject)
  private
    FDataSet: TFDMemTable;
    FCustomerModule: TCustomerModule;
  public
    [Setup]
    procedure Setup;
    [TearDown]
    procedure TearDown;
    // Sample Methods
    // Simple single Test
    [Test]
    procedure TestCalcCaliforniaPersons;
    [Test]
    procedure TestExportToCSV;
  end;

implementation

uses
  System.SysUtils,
  System.Classes,
  System.Hash,
  FireDAC.Stan.Intf,
  FireDAC.Stan.StorageXML,
  ExportServiceU,
  GeocodingServiceU;

procedure TColumbusDatamodulesTests.Setup;
const
  CustomerDataFile = 'customers.xml';
var
  path1: string;
begin
  FDataSet := TFDMemTable.Create(nil);
  FCustomerModule := TCustomerModule.Create(FDataSet, TExportService.Create,
    TGeocodingService.Create);
  path1 := TPath.Combine(TPath.GetDirectoryName(GetModuleName(HInstance)), CustomerDataFile);
  if FileExists(path1) then
    FDataSet.LoadFromFile(path1, sfXML)
  else
    raise Exception.Create('Can''t load test data from ' + CustomerDataFile);
end;

procedure TColumbusDatamodulesTests.TearDown;
begin

  FCustomerModule.Free;
  FDataSet.Free;
end;

procedure TColumbusDatamodulesTests.TestCalcCaliforniaPersons;
begin
  FDataSet.Open;
  Assert.AreEqual(3, FCustomerModule.PeopleInCalifornia);
end;

function GetStrHashSHA1(Str: String): String;
var
  HashSHA: System.Hash.THashSHA1;
begin
  HashSHA := THashSHA1.Create;
  HashSHA.GetHashString(Str);
  result := HashSHA.GetHashString(Str);
end;

procedure TColumbusDatamodulesTests.TestExportToCSV;
var
  Stream: TStringStream;
  Hash: string;
begin
  Stream := TStringStream.Create;
  FCustomerModule.ExportToStream(Stream);
  Hash := GetStrHashSHA1(Stream.DataString);
  Assert.AreEqual('df8199e4b6b9009b8e8070c0acccb6c1b1c290b4', Hash);
  Stream.Free;
end;

initialization

TDUnitX.RegisterTestFixture(TColumbusDatamodulesTests);

end.
