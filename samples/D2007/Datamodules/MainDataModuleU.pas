unit MainDataModuleU;

interface

uses
  SysUtils, Classes, DB,ColumbusCommons,
  ColumbusModuleCustomersU, ColumbusModuleSalesU, ADODB;

type
  TDataModuleMain = class(TDataModule)
    dsrcCustomers: TDataSource;
    ADOConnection1: TADOConnection;
    dsSales: TADOQuery;
    dsCustomers: TADOQuery;
    dsCustomersCUST_NO: TIntegerField;
    dsCustomersCUSTOMER: TStringField;
    dsCustomersCONTACT_FIRST: TStringField;
    dsCustomersCONTACT_LAST: TStringField;
    dsCustomersPHONE_NO: TStringField;
    dsCustomersADDRESS_LINE1: TStringField;
    dsCustomersADDRESS_LINE2: TStringField;
    dsCustomersCITY: TStringField;
    dsCustomersSTATE_PROVINCE: TStringField;
    dsCustomersCOUNTRY: TStringField;
    dsCustomersPOSTAL_CODE: TStringField;
    dsCustomersON_HOLD: TStringField;
    ADODataSet1: TADODataSet;
    procedure DataModuleCreate(Sender: TObject);
  private
    FCustomerModule: TCustomerModule;
    FSalesModule: TSalesModule;
    { Private declarations }
  public
    procedure ExportToFile(const Filename: String);
    procedure CustomerGeocoding;
    function CustomerModule: TColumbusSubject;
    function SalesModule: TColumbusSubject;
  end;

var
  DataModuleMain: TDataModuleMain;

implementation

uses
  ExportServiceU, GeocodingServiceU;

{ %CLASSGROUP 'Vcl.Controls.TControl' }

{$R *.dfm}


procedure TDataModuleMain.CustomerGeocoding;
begin
  FCustomerModule.CustomerGeocode;
end;

function TDataModuleMain.CustomerModule: TColumbusSubject;
begin
  Result := FCustomerModule;
end;

procedure TDataModuleMain.DataModuleCreate(Sender: TObject);
begin
  FCustomerModule := TCustomerModule.Create(dsCustomers, TExportService.Create, TGeocodingService.Create);
  FSalesModule := TSalesModule.Create(dsSales);

end;

procedure TDataModuleMain.ExportToFile(const Filename: String);
begin
  FCustomerModule.ExportToFile(Filename);
end;

function TDataModuleMain.SalesModule: TColumbusSubject;
begin
  Result := FSalesModule;
end;

end.
