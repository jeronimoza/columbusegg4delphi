program ColumbusDatamodulesTests;
{

  Delphi DUnit Test Project
  -------------------------
  This project contains the DUnit test framework and the GUI/Console test runners.
  Add "CONSOLE_TESTRUNNER" to the conditional defines entry in the project options 
  to use the console test runner.  Otherwise the GUI test runner will be used by 
  default.

}

{$IFDEF CONSOLE_TESTRUNNER}
{$APPTYPE CONSOLE}
{$ENDIF}

uses
  Forms,
  TestFramework,
  GUITestRunner,
  TextTestRunner,
  TestColumbusModuleCustomersU in 'TestColumbusModuleCustomersU.pas',
  ColumbusCommons in '..\..\..\..\ColumbusCommons.pas',
  ColumbusModulesLocator in '..\..\..\..\ColumbusModulesLocator.pas',
  ColumbusUIListenerInterface in '..\..\..\..\ColumbusUIListenerInterface.pas',
  ColumbusModuleCustomersU in '..\ColumbusModuleCustomersU.pas',
  ColumbusModuleSalesU in '..\ColumbusModuleSalesU.pas',
  ExportServiceU in '..\ExportServiceU.pas',
  GeocodingServiceU in '..\GeocodingServiceU.pas';

{$R *.RES}

begin
  Application.Initialize;
  if IsConsole then
    TextTestRunner.RunRegisteredTests
  else
    GUITestRunner.RunRegisteredTests;
end.

