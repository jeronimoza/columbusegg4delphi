unit GeocodingServiceU;

interface

type
  IGeocodingService = interface
    ['{EC38CF95-F982-4BFD-9EA8-D3837957D1B1}']
    procedure GetCoords(const City, State: String; out Lat, Lon: Extended);
  end;

  TGeocodingService = class(TInterfacedObject, IGeocodingService)
  public
    procedure GetCoords(const City, State: String; out Lat, Lon: Extended);
  end;

implementation

uses
  IdHTTP, Classes, IdUri, superobject, SysUtils;

const
  GEOCODEURL = 'http://maps.google.com/maps/api/geocode/json?address=';

  { TGeocodingService }

procedure TGeocodingService.GetCoords(const City, State: String; out Lat,
  Lon: Extended);
var
  lHTTP: TIdHTTP;
  lURL: string;
  lResponse: TStringStream;
  lstr: string;
  lJson: ISuperObject;
//  lJObj: TJSONObject;
//  lJGeometry: TJSONObject;
//  lJLocation: TJSONObject;
//  lJNumber: TJSONNumber;
begin
  lHTTP := TIdHTTP.Create;
  try

    //lURL := GEOCODEURL + ',' + TNetEncoding.URL.Encode(City) + ',' + TNetEncoding.URL.Encode(State);
    lurl := TIdURI.URLEncode(UTF8Encode(GEOCODEURL + ',' + City + ',' + State));

//    lResponse := TStringStream.Create('');
    lstr := lHTTP.Get(lURL);
    lJson := SO(lstr);
//    lJObj := TJSONObject.ParseJSONValue(lResp.ContentAsString) as TJSONObject;
//    try
      if lJson.S['status'] = 'OK' then
      begin
        Lat := lJson.A['results'].O[0].O['geometry'].O['location'].C['lat'];
        Lon := lJson.A['results'].O[0].O['geometry'].O['location'].C['lng'];

//        lJGeometry := (lJObj.GetValue('results') as TJSONArray).Items[0].GetValue<TJSONObject>('geometry');
//        lJLocation := lJGeometry.GetValue('location') as TJSONObject;
//        if lJLocation.TryGetValue<TJSONNumber>('lat', lJNumber) then
//          Lat := lJNumber.AsDouble;
//        if lJLocation.TryGetValue<TJSONNumber>('lng', lJNumber) then
//          Lon := lJNumber.AsDouble;
      end
      else
      begin
        raise Exception.Create('Impossibile geocoding: ' + lJson.S['status']);
      end;
//    finally
//      lJObj.Free;
//    end;

  finally
    lHTTP.Free;
  end;
end;

end.
