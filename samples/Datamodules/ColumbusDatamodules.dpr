program ColumbusDatamodules;

uses
  Vcl.Forms,
  MainFormU in 'MainFormU.pas' {MainForm},
  MainDataModuleU in 'MainDataModuleU.pas' {DataModuleMain: TDataModule},
  ColumbusModule.SalesU in 'ColumbusModule.SalesU.pas',
  ExportServiceU in 'ExportServiceU.pas',
  VclUIListener in 'VclUIListener.pas',
  ColumbusUIListenerInterface in '..\..\ColumbusUIListenerInterface.pas',
  ColumbusCommons in '..\..\ColumbusCommons.pas',
  ColumbusModulesLocator in '..\..\ColumbusModulesLocator.pas',
  GeocodingServiceU in 'GeocodingServiceU.pas',
  ColumbusModule2.CustomersU in 'ColumbusModule2.CustomersU.pas';

{$R *.res}

begin
  ReportMemoryLeaksOnShutdown := True;
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TDataModuleMain, DataModuleMain);
  Application.CreateForm(TMainForm, MainForm);
  Application.Run;
end.
